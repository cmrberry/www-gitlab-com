---
layout: markdown_page
title: "Google CloudBuild"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Website
[Google CloudBuild](https://cloud.google.com/cloud-build/)

### Comments
- From GitLab PM
> It is competitive, but I don't see it getting a significant amount of usage. It's been around for awhile and we never really saw it competitively. This rebranding/marketing push may move the needle on awareness, but they take a pretty rigid approach to how you define your builds which will turn people off I think. It's hard to use for common tasks.

### Integrating Google CloudBuild together with GitLab
Google and GitLab partnered to create a demo for Google Next 2018. This showed 2 use cases: 
- Using GitLab for SCM-only and CloudBuild for CI/CD
- Using GitLab CI/CD test and deploy, but offloading the build stage to CloudBuild 

[Full details, as well as links to the demo and sample code](https://about.gitlab.com/2018/07/27/google-next-2018-recap/#google-cloud-build--gitlab-cicd) are on the GitLab blog. 

### Pricing
- [Google Pricing Guide](https://cloud.google.com/cloud-build/pricing)
- First 120 build-minutes per day - Free
- Additional build-minutes - $0.0034 per minute
- Note: Google Cloud Platform costs apply on top of these costs
